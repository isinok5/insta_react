import React from 'react';
import User from './User';

export default function Users() {
    return (
        <div className="right">
            <User
                src="https://www.biography.com/.image/t_share/MTY3MDUxMjkzMjI1OTIwMTcz/brad-pitt-attends-the-premiere-of-20th-century-foxs--square.jpg"
                alt="man"
                name="Brad"
            />
            <div className="users__block">
                <User
                    src="https://www.biography.com/.image/t_share/MTY3MDUxMjkzMjI1OTIwMTcz/brad-pitt-attends-the-premiere-of-20th-century-foxs--square.jpg"
                    alt="man"
                    name="Brad"
                    min
                />
                <User
                    src="https://www.biography.com/.image/t_share/MTY3MDUxMjkzMjI1OTIwMTcz/brad-pitt-attends-the-premiere-of-20th-century-foxs--square.jpg"
                    alt="man"
                    name="Brad"
                    min
                />
                <User
                    src="https://www.biography.com/.image/t_share/MTY3MDUxMjkzMjI1OTIwMTcz/brad-pitt-attends-the-premiere-of-20th-century-foxs--square.jpg"
                    alt="man"
                    name="Brad"
                    min
                />
                <User
                    src="https://www.biography.com/.image/t_share/MTY3MDUxMjkzMjI1OTIwMTcz/brad-pitt-attends-the-premiere-of-20th-century-foxs--square.jpg"
                    alt="man"
                    name="Brad"
                    min
                />
                <User
                    src="https://www.biography.com/.image/t_share/MTY3MDUxMjkzMjI1OTIwMTcz/brad-pitt-attends-the-premiere-of-20th-century-foxs--square.jpg"
                    alt="man"
                    name="Brad"
                    min
                />
            </div>
        </div>
    )
}